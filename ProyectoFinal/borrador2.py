""" Python3 code for inorder successor
and predecessor of tree """
 
# A Binary Tree Nodo
# Utility function to create a new tree nodo
class Nodo:
 
    # Constructor to create a new nodo
    def __init__(self, clave):
        self.clave = clave
        self.hijoIzq = None
        self.hijoDer = None
 
# funcion para encontrar hijoIzquierdo en el arbol
def encontrarHijoIzq(nodo):
    while (nodo != None and nodo.hijoIzq != None):
        nodo = nodo.hijoIzq
    return nodo
 
# funcion para encontrar hijoDerecho en el arbol
def encontrarHijoDer(nodo):
    while (nodo != None and nodo.hijoDer != None):
        nodo = nodo.hijoDer
    return nodo
 
# Funcion Recursiva para encontrar el Sucesor Inorden 
# Cuando el hijo del nodo es None
def encontrarSucesorR(raiz, x ):
 
    if (not raiz):
        return None
    if (raiz == x or (encontrarSucesorR(raiz.hijoIzq, x)) or
                     (encontrarSucesorR(raiz.hijoDer, x))):
        if encontrarSucesorR(raiz.hijoDer, x):
            temp=encontrarSucesorR(raiz.hijoDer, x)
        else:
            temp=encontrarSucesorR(raiz.hijoIzq, x)
        if (temp):
         
            if (raiz.hijoIzq == temp):
             
                print("Inorden Sucesor ",
                            x.clave, end = "")
                print(" es", raiz.clave)
                return None   
        return raiz
    return None
 
# funcion para encontrar el sucesor en inorden 
# de un nodo
def inorderSuccessor(raiz, x):
     
    if (x.hijoDer != None) :
        inorderSucc = encontrarHijoIzq(x.hijoDer)
        print("Inorden Sucesor de", x.clave, "es", end = " ")
        print(inorderSucc.clave)
         
    if (x.hijoDer == None):
        f = 0
        hijoDerMost = encontrarHijoDer(raiz)
 
        # caso3: Si x es el hijoDer mas nodo
        if (hijoDerMost == x):
            print("Sin sucesor en inorden!", "hijoDer mas nodo.")
        else:
            encontrarSucesorR(raiz, x)

def inorderPredecessor(raiz, x):
 
    if (x.hijoIzq != None) :
        inorderSucc = encontrarHijoIzq(x.hijoIzq)
        print("Inorden Predecesor de", x.clave, "es", end = " ")
        print(inorderSucc.clave)

    if (x.hijoIzq == None):
        f = 0
        hijoIzqMost = encontrarHijoIzq(raiz)
 
        if (hijoIzqMost == x):
            print("Sin Predecesor en inorden!", "hijoIzq sin nodo nodo.")
        else:
            encontrarSucesorR(raiz, x)


# Probar Codigo
if __name__ == '__main__':
 
    raiz = Nodo(1)
    raiz.hijoIzq = Nodo(2)
    raiz.hijoDer = Nodo(3)
    raiz.hijoIzq.hijoIzq = Nodo(4)
    raiz.hijoIzq.hijoDer = Nodo(5)
    raiz.hijoDer.hijoDer = Nodo(6)
    raiz.hijoDer.hijoIzq = Nodo(8)
    raiz.hijoIzq.hijoIzq.hijoIzq = Nodo(19)
    nodo = raiz.hijoIzq.hijoDer
    # Case 1
    inorderSuccessor(raiz, raiz.hijoDer)
    inorderSuccessor(raiz, raiz.hijoIzq)
    inorderSuccessor(raiz, nodo)
    # case 2
    inorderSuccessor(raiz, raiz.hijoIzq.hijoIzq)
    inorderPredecessor(raiz, raiz.hijoDer)
    inorderPredecessor(raiz, raiz.hijoDer.hijoDer)
 
 
    # case 3
    inorderSuccessor(raiz, raiz.hijoDer.hijoDer)
 
# This code is contributed
# by SHUBHAMSINGH10

#Predecesor y sucesor

# def buscarPadre(raiz, clave):
#     if raiz == None:  # si la raiz es None, no existe el nodo
#         return None
#     if (raiz.hijoIzq != None and raiz.hijoIzq.clave == clave):  
#         # si el hijo izquierdo del nodo es igual a la clave del nodo a buscar, se retorna el nodo padre
#         return raiz
#     # si el hijo derecho del nodo es igual a la clave del nodo a buscar,
#     # se retorna el nodo padre
#     elif (raiz.hijoDer != None and raiz.hijoDer.clave == clave):  
#         return raiz
#     elif (clave < raiz.clave):                        # si el dato del nodo es menor que el dato del nodo a buscar,
#         return buscarPadre(raiz.hijoIzq, clave)       # se mueve por el arbol a la izquierda y se busca el nodo padre
#     else:                                             # en el arbol de manera recursiva, recibiendo el hijo izquierdo 
#         return buscarPadre(raiz.hijoDer, clave)       # hasta encontrar el nodo padre y esto lo hace tambien con el 
#                                                       # Hijo derecho

# def sucesor(raiz, clave):
#     nodo = t.__buscar(raiz, clave)
#     if nodo == None:
#         return None
#     if nodo.hijoDer != None:
#         return t.getMin(nodo.hijoDer)
#     y = buscarPadre(raiz, clave)
#     while y != None and y.hijoDer != nodo:
#         nodo = y
#         y = buscarPadre(raiz, y.clave)
#     return y


# def predecesor(raiz, clave):
#     nodo = t.__buscar(raiz, clave)
#     if nodo == None:
#         return None
#     if nodo.hijoIzq != None:
#         return t.getMax(nodo.hijoIzq)
#     y = buscarPadre(raiz, clave)
#     while y != None and y.hijoIzq != nodo:
#         nodo = y
#         y = buscarPadre(raiz, y.clave)
#     return y