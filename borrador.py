    def __agregar_recursivo(self, nodo, clave):
        if clave < nodo.clave:
            if nodo.hijoIzq is None:
                nodo.hijoIzq = Nodo(clave, None)
            else:
                self.__agregar_recursivo(nodo.getHijoIzq(), clave)
        else:
            if nodo.hijoDer is None:
                nodo.hijoDer = Nodo(clave, None)
            else:
                self.__agregar_recursivo(nodo.getHijoDer(), clave)

    def agregar(self, dato):
        self.__agregar_recursivo(self.raiz, dato)
       print(t.agregar(31))