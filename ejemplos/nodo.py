class Nodo:
    def __init__(self, clave):
        # "clave" puede ser de cualquier tipo, incluso un objeto si se sobrescriben los operadores de comparación
        self.clave = clave
        self.izquierda = None
        self.derecha = None