# paso 1: Cree un generador de 100.000 números distintos de manera
# aleatoria, guárdelos en un archivo txt.
import json
import random  # Importamos Random


lista = []  # Creamos la lista Vacía donde almacenaremos los 10000 numeros
# para luego guardarlos en el txt

#Funcion para generar numeros aleatorios
def devolverNumeros():

    mi_lista =[]
    Fin = int(input("Por favor ingrese la cantidad de numeros a generar: "))

    hasta = 200000   #pow(8, 10)
    for i in range(1, Fin+1):
        mi_lista.append(random.sample(0, hasta))
        unicos = list(dict.fromkeys(mi_lista))
    return unicos
#funcion para ingresar numeros a archivo.txt
def agregarNumeros():
    lista = devolverNumeros()
    try:
        archivo = open('numeros.txt','w')
        archivo.writelines("%s\n" % place for place in lista)
    except Exception as e:
        print(e)
    finally:
        archivo.close()

agregarNumeros()

# Function to do insertion sort
def insertionSort(arreglo):
 
    # Traverse through 1 to len(arreglo)
    for i in range(1, len(arreglo)):
 
        key = arreglo[i]
 
        # Move elements of arreglo[0..i-1], that are
        # greater than key, to one position ahead
        # of their current position
        j = i-1
        while j >=0 and key < arreglo[j] :
                arreglo[j+1] = arreglo[j]
                j -= 1
        arreglo[j+1] = key

#Definir funcion para hallar la altura del arbol
