class Nodo:
    
    def __init__(self, clave, padre=None, hijoIzq=None, hijoDer=None):
        self.clave = clave
        self.padre = padre
        self.hijoIzq = hijoIzq
        self.hijoDer = hijoDer

    def setPadre(self, padre):
        self.padre = padre
    def setHijoIzq(self, hijoIzq):
        self.hijoIzq = hijoIzq
    def setHijoDer(self, hijoDer):
        self.hijoDer = hijoDer
    
raiz = Nodo(6)
nodo1 = Nodo(7, raiz)
nodo2 = Nodo(5, raiz)
nodo3 = Nodo(8, nodo1)
nodo4 = Nodo(10, nodo1)
nodo5 = Nodo(2, nodo2)
nodo6 = Nodo(3, nodo4)

# asignacion hijos

raiz.setHijoIzq(nodo1)
raiz.setHijoDer(nodo2)

nodo1.setHijoIzq(nodo3)
nodo1.setHijoDer(nodo4)

nodo2.setHijoDer(nodo5)

nodo4.setHijoIzq(nodo6)

print(raiz.hijoIzq.hijoDer.clave)

def altura(nodo, nivel):
    if(nodo==None):
        return nivel
    else:
       return max(altura(nodo.hijoIzq, nivel + 1), altura(nodo.hijoDer, nivel + 1))

print(altura(raiz,-1))

print()
