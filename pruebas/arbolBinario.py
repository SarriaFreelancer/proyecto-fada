# from __future__ import print_function
# from arbol import Nodo

# Declaramos la clase 'Nodo'
class Nodo:

    def __init__(self, clave, padre=None, hijoIzq=None, hijoDer=None):
        self.clave = clave
        self.padre = padre
        self.hijoIzq = hijoIzq
        self.hijoDer = hijoDer
       

    # Métodos para asignar nodos
    def getClave(self):
        return self.clave

    def setClave(self, clave):
        self.clave = clave

    def getHijoIzq(self):
        return self.hijoIzq

    def setHijoIzq(self, hijoIzq):
        self.hijoIzq = hijoIzq

    def getHijoDer(self):
        return self.hijoDer

    def setHijoDer(self, hijoDer):
        self.hijoDer = hijoDer

    def getPadre(self):
        return self.padre

    def setPadre(self, padre):
        self.padre = padre


class BinarySearchTree:

    def __init__(self, raiz = None):
        self.raiz = raiz

    #Funcion para insertar un nuevo dato
    def insert(self, clave):
        # Creamos un nuevo nodo
        nuevo_Nodo = Nodo(clave, None)
        # Si el árbol esta vacio
        if self.empty():
            self.raiz = nuevo_Nodo
        else:
            # Si el árbol no esta vacio
            nodo_Actual = self.raiz
            while nodo_Actual is not None:
                padre_nodo = nodo_Actual
                if nuevo_Nodo.getClave() < nodo_Actual.getClave():
                    nodo_Actual = nodo_Actual.getHijoIzq()
                else:
                    nodo_Actual = nodo_Actual.getHijoDer()
            if nuevo_Nodo.getClave() < padre_nodo.getClave():
                padre_nodo.setHijoIzq(nuevo_Nodo)
            else:
                padre_nodo.setHijoDer(nuevo_Nodo)
            nuevo_Nodo.setPadre(padre_nodo)      
    
    # Función de borrado de un arbol
    def borrar(self, clave):
        if (not self.empty()):
            nodo = self.getNodo(clave)
            if(nodo is not None):
                if(nodo.getHijoIzq() is None and nodo.getHijoDer() is None):
                    self.__reassignNodos(nodo, None)
                    nodo = None
                elif(nodo.getHijoIzq() is None and nodo.getHijoDer() is not None):
                    self.__reassignNodos(nodo, nodo.getHijoDer())
                elif(nodo.getHijoIzq() is not None and nodo.getHijoDer() is None):
                    self.__reassignNodos(nodo, nodo.getHijoIzq())
                else:
                    tmpNodo = self.getMax(nodo.getHijoIzq())
                    self.borrar(tmpNodo.getClave())
                    nodo.setClave(tmpNodo.getClave())
    
    def getNodo(self, clave):
        nodo_Actual = None
        if(not self.empty()):
            nodo_Actual = self.getRaiz()
            while nodo_Actual is not None and nodo_Actual.getClave() is not clave:
                if clave < nodo_Actual.getClave():
                    nodo_Actual = nodo_Actual.getHijoIzq()
                else:
                    nodo_Actual = nodo_Actual.getHijoDer()
        return nodo_Actual
    
    #Funcion para hayar la clave maxima
    def getMax(self, raiz = None):
        if(raiz is not None):
            nodo_Actual = raiz
        else:
            nodo_Actual = self.getRaiz()
        if(not self.empty()):
            while(nodo_Actual.getHijoDer() is not None):
                nodo_Actual = nodo_Actual.getHijoDer()
        return nodo_Actual

    #Funcion para hayar la clave minima
    def getMin(self, raiz = None):
        if(raiz is not None):
            nodo_Actual = raiz
        else:
            nodo_Actual = self.getRaiz()
        if(not self.empty()):
            nodo_Actual = self.getRaiz()
            while(nodo_Actual.getHijoIzq() is not None):
                nodo_Actual = nodo_Actual.getHijoIzq()
        return nodo_Actual

    def empty(self):
        if self.raiz is None:
            return True
        return False

    def __InOrderTraversal(self, nodo_Actual):
        listaNodo = []
        if nodo_Actual is not None:
            listaNodo.insert(0, nodo_Actual)
            listaNodo = listaNodo + self.__InOrderTraversal(nodo_Actual.getHijoIzq())
            listaNodo = listaNodo + self.__InOrderTraversal(nodo_Actual.getHijoDer())
        return listaNodo

    def getRaiz(self):
        return self.raiz

    def __ishijoDerChildren(self, nodo):
        if(nodo == nodo.getPadre().getHijoDer()):
            return True
        return False

    #Funcion para reasignar nodos despues de eliminar
    def __reassignNodos(self, nodo, nuevoHijo):
        if(nuevoHijo is not None):
            nuevoHijo.setPadre(nodo.getPadre())
        if(nodo.getPadre() is not None):
            if(self.__ishijoDerChildren(nodo)):
                nodo.getPadre().setHijoDer(nuevoHijo)
            else:
                nodo.getPadre().setHijoIzq(nuevoHijo)

    def traversalTree(self, traversalFunction = None, raiz = None):
        if(traversalFunction is None):
            return self.__InOrderTraversal(self.raiz)
        else:
            return traversalFunction(self.raiz)

    #Funcion  para recorrer y obtener los valores ordenados del arbol
    #en inorden Tranversal
    def __str__(self):
        list = self.__InOrderTraversal(self.raiz)
        str = ''
        for x in list:
            str = str + ' ' + x.getClave().__str__()
        return str

def InPreOrder(nodo_Actual):
    listaNodo = []
    if nodo_Actual is not None:
        listaNodo = listaNodo + InPreOrder(nodo_Actual.getHijoIzq())
        listaNodo.insert(0, nodo_Actual.getClave())
        listaNodo = listaNodo + InPreOrder(nodo_Actual.getHijoDer())
    return listaNodo

# Instancia del árbol binario de búsqueda
t = BinarySearchTree()

#Funcion para Insertar Valores al Arbol
def insertarValores():
    #Insertamos los elementos al arbol
    lista = [3,6,9,2,7,4,8,5,10,1]
    #lista = [6,7,5,8,10,2,3] prueba con arbol creado por maestro

    for i in lista:
        t.insert(i)
    # Obtenemos todos los elementos del árbol en preorden
    # list = t.traversalTree(InPreOrder, t.raiz)
    return t.__str__()
#Funcion para insertar un valor Manualmente
def insertarValoresManual():
    cantidad = int(input('Cuantos valores desea ingresar?: '))
    cont = 1
    while cont <= cantidad:
        valor = int(input(f'Ingrese el valor {cont}: '))
        t.insert(valor)
        cont += 1
    return t.__str__()


#Funcion para hayar la clave Maximaria
def claveMaxima():
    if(not t.empty()):
        return f'La Clave Máxima Es: {t.getMax().getClave()}'
        
#Funcion para hayar la clave Minima
def claveMinima():
    if(not t.empty()):
        return f'La Clave Mínima Es: {t.getMin().getClave()}'

#Funcion para borrar un nodo especifico
def borrarValores():
    nuBorrar = int(input('cuantos datos desea eliminar: '))
    if nuBorrar == 0:
        print('Valor ingresado es menor o igual a 0')
    else:
        cont = 1
        while cont <= nuBorrar:
            valor = int(input(f'Ingrese el numero {cont} a borrar: '))
            t.borrar(valor)
            cont += 1
    return t.__str__()

#Funcion para buscar un dato en el arbol
def buscarDato():
    dato = int(input('Por favor ingrese el dato a buscar: '))
    if(t.getNodo(dato) is not None):
        print(f'El elemento {dato} existe')
    else:
        print(f'El elemento {dato} no existe')
    return t.__str__()

#Funcion para calcular la altura del arbol
#se calcula pasando el nodo raiz i el nivel -1
def altura(nodo, nivel):
    if(nodo==None):
        return nivel
    else:
        return max(altura(nodo.hijoIzq, nivel + 1), altura(nodo.hijoDer, nivel + 1))
def imprimirAltura():
    return f'La Altura del Arbol es: {altura(t.raiz,-1)}'

def __len__(self):
        if self == None:
            return 0
        else:
            size = 1
            if self.right:
                size += self.right.__len__()
            if self.left:
                size += self.left.__len__()
            return size



print('==================================')
if __name__ == '__main__':
    print(insertarValores())
    print(claveMaxima())
    print(claveMinima())
    print(borrarValores())
    print(buscarDato())
    print(insertarValoresManual())
    print(imprimirAltura())
    print(claveMaxima())
    