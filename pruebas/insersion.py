# Python program for implementation of Insertion Sort
 
# Function to do insertion sort
def insertionSort(arreglo):
 
    # Traverse through 1 to len(arreglo)
    for i in range(1, len(arreglo)):
 
        key = arreglo[i]
 
        # Move elements of arreglo[0..i-1], that are
        # greater than key, to one position ahead
        # of their current position
        j = i-1
        while j >=0 and key < arreglo[j] :
                arreglo[j+1] = arreglo[j]
                j -= 1
        arreglo[j+1] = key
 
 
# Driver code to test above
arreglo = [12, 11, 13, 5, 6, 14, 17, 8, 1]
insertionSort(arreglo)
print ("Sorted array is:")
for i in range(len(arreglo)):
    print ("%d" %arreglo[i])