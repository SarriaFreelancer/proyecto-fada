# class Nodo:
#     def __init__(self, dato):
#         # "dato" puede ser de cualquier tipo, incluso un objeto si se sobrescriben los operadores de comparación
#         self.dato = dato
#         self.izquierda = None
#         self.derecha = None

class node(object):
    def __init__(self, data):
        self.data = data
        self.children = []

    def add_child(self, obj):
        self.children.append(obj)

#A continuación defino todos los nodos
a = node(1)
b = node(2)
c = node(3)
d = node(4)
e = node(5)
f = node(6)
g = node(7)
h = node(8)
i = node(9)
j = node(10)
k = node(11)
l = node(12)
ll = node(13)
m = node(14)
n = node(15)
o = node(16)
# A continuación defino las relaciones de parentesco
a.add_child(b)
a.add_child(c)
a.add_child(d)
b.add_child(e)
b.add_child(f)
b.add_child(g)
d.add_child(h)
d.add_child(i)
d.add_child(j)
h.add_child(n)
h.add_child(o)
e.add_child(k)
e.add_child(l)
e.add_child(ll)
e.add_child(m)

cadena = ('1 - 2 3 4 * 2 - 5 6 7 * 5 - 11 12 13 14 * 11 - * '
       '12 - * 13 - * 14 - * 6 - * 7 - * 3 - * 4 - 8 9 10 * '
       '8 - 15 16 * 15 - * 16 - * 9 - * 10 - *')
#1. Genero todos los nodos
#1.2 Reduzco la cadena a elementos que no se repitan
cadena2 = list(set(cadena.split()))
#print(cadena2)
#1.3 Elimino de la cadena los símbolos '*' y '-'
cadena2.remove("*")
cadena2.remove("-")
#print(cadena2)
#1.4 Genero todos los nodos
listanodos = []
i = 0
while i < len(cadena2):
    listanodos = listanodos + [node(cadena2[i])]
    i = i + 1
#2 Vinculo los nodos hijos a los padres
# ¿Cómo lo hago?

cad = ('1 - 2 3 4 * 2 - 5 6 7 * 5 - 11 12 13 14 * 11 - * '
       '12 - * 13 - * 14 - * 6 - * 7 - * 3 - * 4 - 8 9 10 * '
       '8 - 15 16 * 15 - * 16 - * 9 - * 10 - *')

class Node(object):
    def __init__(self, data):
        self.data = data
        self.children = []

    def add_child(self, obj):
        self.children.append(obj)


nodes = [n.split('-') for n in cad.split('*')][:-1]
nodes = {int(parent): [int(child) for child in children.split()]
                                      for parent, children in nodes}
nodes_instances = {n: Node(n) for n in nodes}

for parent, children in nodes.items():
    for child in children:
        nodes_instances[parent].add_child(nodes_instances[child])

listanodos = nodes_instances.values()

nodes = {int(parent): [int(child) for child in children.split()] for parent, children in nodes}

nodes_instances = {n: Node(n) for n in nodes}

for n in listanodos:
    print('{:<2} ---> {}'.format(n.data, (', '.join(str(c.data) for c in n.children) if n.children else None)))