

class Arbol:
    # Funciones privadas
    def __init__(self, dato):
        self.raiz = Nodo(dato)

    def __agregar_recursivo(self, nodo, dato):
        if dato < nodo.dato:
            if nodo.izquierda is None:
                nodo.izquierda = Nodo(dato)
            else:
                self.__agregar_recursivo(nodo.izquierda, dato)
        else:
            if nodo.derecha is None:
                nodo.derecha = Nodo(dato)
            else:
                self.__agregar_recursivo(nodo.derecha, dato)

    def __inorden_recursivo(self, nodo):
        if nodo is not None:
            self.__inorden_recursivo(nodo.izquierda)
            print(nodo.dato, end=", ")
            self.__inorden_recursivo(nodo.derecha)

    def __preorden_recursivo(self, nodo):
        if nodo is not None:
            print(nodo.dato, end=", ")
            self.__preorden_recursivo(nodo.izquierda)
            self.__preorden_recursivo(nodo.derecha)

    def __postorden_recursivo(self, nodo):
        if nodo is not None:
            self.__postorden_recursivo(nodo.izquierda)
            self.__postorden_recursivo(nodo.derecha)
            print(nodo.dato, end=", ")

    def __buscar(self, nodo, busqueda):
        if nodo is None:
            return None
        if nodo.dato == busqueda:
            return nodo
        if busqueda < nodo.dato:
            return self.__buscar(nodo.izquierda, busqueda)
        else:
            return self.__buscar(nodo.derecha, busqueda)

    # Funciones públicas

    def agregar(self, dato):
        self.__agregar_recursivo(self.raiz, dato)

    def inorden(self):
        print("Imprimiendo árbol inorden: ")
        self.__inorden_recursivo(self.raiz)
        print("")

    def preorden(self):
        print("Imprimiendo árbol preorden: ")
        self.__preorden_recursivo(self.raiz)
        print("")

    def postorden(self):
        print("Imprimiendo árbol postorden: ")
        self.__postorden_recursivo(self.raiz)
        print("")

    def buscar(self, busqueda):
        return self.__buscar(self.raiz, busqueda)


# Función para probar las clases
#def testBinarySearchTree():
    '''
    Ejemplo
                  8
                 / 
                3   10
               /     
              1   6    14
                 /    /
                4   7 13 
    '''

    '''
    Ejemplo luego del borrado
                  7
                 / 
                1   4

    '''
    # # Instancia del árbol binario de búsqueda
    #t = BinarySearchTree()
    # #Insertamos los elementos al arbol
    # t.insert(8)
    # t.insert(3)
    # t.insert(6)
    # t.insert(1)
    # t.insert(10)
    # t.insert(14)
    # t.insert(13)
    # t.insert(4)
    # t.insert(7)

    # print(t.__str__())

    # if(t.getNodo(21) is not None):
    #     print("El elemento 21 existe")
    # else:
    #     print("El elemento 21 no existe")


    
    # if(not t.empty()):
    #     print(("Valor Max: ", t.getMax().getClave()))
    #     print(("Valor Min: ", t.getMin().getClave()))
    
    # t.borrar(13)
    # t.borrar(10)
    # t.borrar(8)
    # t.borrar(3)
    # t.borrar(6)
    # t.borrar(14)

    # # Obtenemos todos los elementos del árbol en preorden
    # list = t.traversalTree(InPreOrder, t.raiz)
    # for x in list:
    #     print(x)
        