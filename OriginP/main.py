"""
    https://parzibyte.me/blog
"""

from arbol import Arbol


 #Aquí tienes en "nodo" toda la información del nodo. Tanto su izquierda, derecha, dato y otros atributos que le hayas agregado

arbol_numeros = Arbol(5)
arbol_numeros.agregar(1984)
arbol_numeros.agregar(60)
arbol_numeros.agregar(10)
arbol_numeros.agregar(20)
arbol_numeros.agregar(10)
arbol_numeros.agregar(25)
arbol_numeros.agregar(59)
arbol_numeros.agregar(64)
arbol_numeros.agregar(10)
arbol_numeros.agregar(19)
arbol_numeros.agregar(23)
arbol_numeros.agregar(18)
arbol_numeros.agregar(1)
arbol_numeros.agregar(2013)
arbol_numeros.preorden()
arbol_numeros.inorden()
arbol_numeros.postorden()

busqueda=int(input("Ingresa un número para buscar en el árbol: "))
n = arbol_numeros.buscar(busqueda)
if n is None:
     print(f"{busqueda} no existe")
else:
     print(f"{busqueda} sí existe")


#Imprimir arbol asignando la Raiz desde afuera
lista = [3,6,9,2,7,4,8,5,10,1]
#arbol_numeros = Arbol(lista[0])
for i in lista:
    arbol_numeros.agregar(i)
arbol_numeros.preorden()
arbol_numeros.inorden()
arbol_numeros.postorden()